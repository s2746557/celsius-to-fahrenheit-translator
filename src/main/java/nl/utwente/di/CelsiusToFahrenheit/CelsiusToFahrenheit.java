package nl.utwente.di.CelsiusToFahrenheit;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class CelsiusToFahrenheit extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Transition transition;

    public void init() throws ServletException {
        transition = new Transition();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Degree Transition";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Degree in celsius: " +
                request.getParameter("cel") + "\n" +
                "  <P>Degree in Fahrenheit: " +
                transition.fromCelsius(request.getParameter("cel")) +
                "</BODY></HTML>");
    }


}