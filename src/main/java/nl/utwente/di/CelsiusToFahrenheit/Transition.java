package nl.utwente.di.CelsiusToFahrenheit;

public class Transition {
    public double fromCelsius(String degree){
        double newDeg;
        newDeg = (Double.parseDouble(degree) * 9 / 5)+ 32;
        return newDeg;
    }
}
